FROM centos:7

RUN yum install python3 python3-pip -y && mkdir /python_api
COPY . /python_api
RUN pip3 install -r requirements.txt
WORKDIR /python_api
CMD ["python3", "python-api.py"]
